Application pour détecter les données saisies non réglementaires

## Install

Just clone the repo:

```
github.com/romainlouvet/detect.git
```

Generate JAR with maven goal:

```
mvn package
```

That's it. Its ready to go.

## Run

Run JAR:

```
java -jar mobileagri-VERSION.jar
```

## License

See LICENSE file for details.
