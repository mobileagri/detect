package com.mobileagri.detect.Service;

import com.mobileagri.detect.dao.ParcelleDao;
import com.mobileagri.detect.entity.Parcelle;
import com.mobileagri.detect.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("parcelleService")
public class ParcelleService {

    private final Logger logger = LoggerFactory.getLogger(ParcelleService.class);

    @Autowired
    private ParcelleDao parcelleDao;


    public Double checkAssolement(User user) {
        Double ret = 0.0;

        Double nbParcelleTotale = (double) parcelleDao.selectByUserId(user.getId()).size();
        Double nbParcelleVide = (double) parcelleDao.selectByUserIdAndWithoutCulture(user.getId()).size();

        if (nbParcelleTotale > 0)
            ret = 100 - ((nbParcelleVide / nbParcelleTotale) * 100);

        return ret;
    }

    public Double checkPlanFumure(User user) {
        Double ret = 0.0;

        Double nbParcelleTot = (double) getParcelleByUser(user).size();
        Double nbParcelleWithRendement = (double) getParcelleByRendement(user).size();

        if (nbParcelleTot > 0)
            ret = (nbParcelleWithRendement / nbParcelleTot) * 100;

        return ret;
    }

    public List<Parcelle> getParcelleByUser(User user) {
        return parcelleDao.selectByUserIdAndWithCulture(user.getId());
    }

    public List<Parcelle> getParcelleByRendement(User user) {
        return parcelleDao.selectByObjectifRendement(user.getId());
    }


}
