package com.mobileagri.detect.Service;

import com.mobileagri.detect.dao.UserDao;
import com.mobileagri.detect.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("userService")
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private DonneeService donneeService;

    @Autowired
    private ParcelleService parcelleService;


    public List<User> getAll() {
        return userDao.getAllUser();
    }

    public User getUser(int id) {
        return userDao.selectUserById(id);
    }

    public User getUser(String login) {
        return userDao.selectUserByName(login);
    }

    /**
     * Return list of same user with different year
     *
     * @param idUser to match
     * @return List<User>
     */
    public List<User> getAll(int idUser) {
        List<User> userList = new ArrayList<>();
        User user = getUser(idUser);

        String loginMatch = user.getLogin();

        String loginToSearch = loginMatch.split("2")[0];

        List<User> listAllUser = getAll();
        for (User u : listAllUser) {
            if (u.getLogin().split("2")[0].equals(loginToSearch) && u.getLogin().contains("20")) {
                userList.add(u);
            }
        }
        return userList;
    }

    public List<User> getAllAgriUser() {
        return userDao.getAllAgriUser();
    }

    /**
     * Calc Ratio nb Produit / nb Parcelle by one user
     *
     * @param user agri
     */
    public Double calcRatioProduitParcelle(User user) {


        Double nbParcelle = (double) parcelleService.getParcelleByUser(user).size();
        Double nbProduit = (double) donneeService.selectDataByUser(user).size();

        Double ratioProduitParcelle = 0.0;
        if (nbParcelle > 0)
            ratioProduitParcelle = nbProduit / nbParcelle;

        return ratioProduitParcelle;
    }

    /**
     * Get average for list of user
     *
     * @param user agri
     * @return ratio
     */
    public Double getAverageRatioAllUser(User user) {
        Double somme = 0.0;
        Double ret = 0.0;
        int nb = 0;
        for (User u : this.getAll(user.getId())) {
            if (!u.getLogin().equals(user.getLogin())) {
                somme += this.calcRatioProduitParcelle(u);

                nb++;
            }
        }

        if (nb > 0)
            ret = somme / nb;
        return ret;
    }

    /**
     * Check carnet de plaine in % by one user
     *
     * @param user agri
     * @return avancement carnet de plaine
     */
    public Double checkCarnetDePlaine(User user) {
        Double ret = 0.0;

        Double actualCampaign = calcRatioProduitParcelle(user);
        Double oldCampaign = getAverageRatioAllUser(user);

        if (oldCampaign > 0)
            ret = ((actualCampaign / oldCampaign) * 100);

        if (ret > 100.0)
            return 100.0;

        return ret;
    }

    public void saveUser(User user) {
        userDao.saveUser(user);
    }
}
