package com.mobileagri.detect.Service;

import com.mobileagri.detect.dao.UserDao;
import com.mobileagri.detect.dao.product.FongicideDao;
import com.mobileagri.detect.dao.product.HerbicideDao;
import com.mobileagri.detect.dao.product.InsecticideDao;
import com.mobileagri.detect.dao.product.RegulateurDao;
import com.mobileagri.detect.entity.AuthorizationEphy;
import com.mobileagri.detect.entity.User;
import com.mobileagri.detect.entity.product.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("appService")
public class AppService {

    private final Logger logger = LoggerFactory.getLogger(AppService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private DonneeService donneeService;

    @Autowired
    private ParcelleService parcelleService;

    @Autowired
    private EPhyCSVService ePhyCSVService;

    @Autowired
    private UserService userService;

    @Autowired
    private HerbicideDao herbicideDao;

    @Autowired
    private FongicideDao fongicideDao;

    @Autowired
    private InsecticideDao insecticideDao;

    @Autowired
    private RegulateurDao regulateurDao;

    public void checkAvancement() {

        for (User user : userService.getAllAgriUser()) {
            logger.info(user.getLogin());


            logger.info("---->Resultat assolement : " + Math.round(parcelleService.checkAssolement(user)));
            logger.info("---->Resultat Carnet de plaine : " + Math.round(userService.checkCarnetDePlaine(user)));
            logger.info("---->Resultat Plan fumure : " + Math.round(parcelleService.checkPlanFumure(user)));

            Double resultat = parcelleService.checkAssolement(user) + userService.checkCarnetDePlaine(user) + parcelleService.checkPlanFumure(user);
            resultat = resultat / 3;
            logger.info("*****" + Math.round(resultat));
            user.setAvancement(resultat);
            user.setAvancementAssolement(parcelleService.checkAssolement(user));
            user.setAvancementCarnet(userService.checkCarnetDePlaine(user));
            user.setAvancementFumure(parcelleService.checkPlanFumure(user));
            userService.saveUser(user);
        }
    }

    public void checkWarningData(String pathCSV) {
        List<AuthorizationEphy> authorizationEphyList = ePhyCSVService.readData(pathCSV);

        List<Herbicide> herbicideList = herbicideDao.getAll();
        List<Fongicide> fongicideList = fongicideDao.getAll();
        List<Insecticide> insecticideList = insecticideDao.getAll();
        List<Regulateur> regulateurList = regulateurDao.getAll();

        List<Product> productList = new ArrayList<>();
        productList.addAll(fongicideList);
        productList.addAll(herbicideList);
        productList.addAll(insecticideList);
        productList.addAll(regulateurList);

        for (Product product : productList) {

            String amm = product.getAmm();

            if (amm.contains("-")) {
                amm = amm.split("-")[0];
            }

            updateStatut(authorizationEphyList, product, amm);
        }
    }

    private void updateStatut(List<AuthorizationEphy> authorizationEphyList, Product product, String amm) {

        if (product.getClass().getName().equals(Fongicide.class.getName())) {
            Fongicide fongicide;
            fongicide = (Fongicide) product;
            updateStatut(authorizationEphyList, fongicide, amm);
        }

        if (product.getClass().getName().equals(Herbicide.class.getName())) {
            Herbicide herbicide;
            herbicide = (Herbicide) product;
            updateStatut(authorizationEphyList, herbicide, amm);
        }

        if (product.getClass().getName().equals(Insecticide.class.getName())) {
            Insecticide insecticide;
            insecticide = (Insecticide) product;
            updateStatut(authorizationEphyList, insecticide, amm);
        }

        if (product.getClass().getName().equals(Regulateur.class.getName())) {
            Regulateur regulateur;
            regulateur = (Regulateur) product;
            updateStatut(authorizationEphyList, regulateur, amm);
        }

    }

    private void updateStatut(List<AuthorizationEphy> authorizationEphyList, Fongicide fongicide, String amm) {
        for (AuthorizationEphy authorizationEphy : ePhyCSVService.findByAMM(authorizationEphyList, amm)) {
            logger.info("Fongicide-->" + authorizationEphy.getNomCommercial() + " / " + authorizationEphy.getStatut() + " / " + authorizationEphy.getDateRetraitDefinitif());
            fongicide.setStatut(authorizationEphy.getStatut());

            if (!authorizationEphy.getDateRetraitDefinitif().equals("NULL"))
                fongicide.setDate_retrait(authorizationEphy.getDateRetraitDefinitif());
            fongicideDao.save(fongicide);
        }
    }

    private void updateStatut(List<AuthorizationEphy> authorizationEphyList, Herbicide herbicide, String amm) {
        for (AuthorizationEphy authorizationEphy : ePhyCSVService.findByAMM(authorizationEphyList, amm)) {
            logger.info("Herbicide-->" + authorizationEphy.getNomCommercial() + " / " + authorizationEphy.getStatut() + " / " + authorizationEphy.getDateRetraitDefinitif());
            herbicide.setStatut(authorizationEphy.getStatut());

            if (!authorizationEphy.getDateRetraitDefinitif().equals("NULL"))
                herbicide.setDate_retrait(authorizationEphy.getDateRetraitDefinitif());
            herbicideDao.save(herbicide);
        }
    }

    private void updateStatut(List<AuthorizationEphy> authorizationEphyList, Insecticide insecticide, String amm) {
        for (AuthorizationEphy authorizationEphy : ePhyCSVService.findByAMM(authorizationEphyList, amm)) {
            logger.info("Insecticide-->" + authorizationEphy.getNomCommercial() + " / " + authorizationEphy.getStatut() + " / " + authorizationEphy.getDateRetraitDefinitif());
            insecticide.setStatut(authorizationEphy.getStatut());

            if (!authorizationEphy.getDateRetraitDefinitif().equals("NULL"))
                insecticide.setDate_retrait(authorizationEphy.getDateRetraitDefinitif());
            insecticideDao.save(insecticide);
        }
    }

    private void updateStatut(List<AuthorizationEphy> authorizationEphyList, Regulateur regulateur, String amm) {
        for (AuthorizationEphy authorizationEphy : ePhyCSVService.findByAMM(authorizationEphyList, amm)) {
            logger.info("Regulateur-->" + authorizationEphy.getNomCommercial() + " / " + authorizationEphy.getStatut() + " / " + authorizationEphy.getDateRetraitDefinitif());
            regulateur.setStatut(authorizationEphy.getStatut());

            if (!authorizationEphy.getDateRetraitDefinitif().equals("NULL"))
                regulateur.setDate_retrait(authorizationEphy.getDateRetraitDefinitif());
            regulateurDao.save(regulateur);
        }
    }
}
