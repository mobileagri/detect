package com.mobileagri.detect.Service;

import com.mobileagri.detect.entity.AuthorizationEphy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("EPhyCSVService")
public class EPhyCSVService {

    private final Logger logger = LoggerFactory.getLogger(EPhyCSVService.class);


    public List<AuthorizationEphy> readData(String csvFile) {
//        String csvFile = "/Users/romainlouvet/Sites/detect/src/main/resources/fichierPhyto2.csv";
        List<AuthorizationEphy> authorizationEphyList = new ArrayList<>();
        BufferedReader br = null;
        String line;
        String csvSplitBy = ",";

        try {
            br = new BufferedReader((new FileReader(csvFile)));

            AuthorizationEphy authorizationEphy;
            int i = 0;
            while ((line = br.readLine()) != null) {
                if (i > 0) {
                    String[] produit = line.split(csvSplitBy);
                    authorizationEphy = new AuthorizationEphy();
                    authorizationEphy.setNumAmm(produit[0].replace("\"", ""));
                    authorizationEphy.setDatePremiereAuthorisation(produit[1].replace("\"", ""));
                    authorizationEphy.setDateEcheanceAuthorization(produit[2].replace("\"", ""));
                    authorizationEphy.setDateRetraitDefinitif(produit[3].replace("\"", ""));
                    authorizationEphy.setStatut(produit[4].replace("\"", ""));
                    authorizationEphy.setNomCommercial(produit[5].replace("\"", ""));
                    authorizationEphy.setTypeCommercial(produit[6].replace("\"", ""));
                    authorizationEphy.setEntreprise(produit[7].replace("\"", ""));

                    authorizationEphyList.add(authorizationEphy);
                }
                i++;

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return authorizationEphyList;
    }

    public List<AuthorizationEphy> findByAMM(List<AuthorizationEphy> allAuthorizationEphyList, String amm) {
        List<AuthorizationEphy> authorizationEphyList = new ArrayList<>();

        for (AuthorizationEphy produit : allAuthorizationEphyList) {
            if (produit.getNumAmm().equals(amm))
                authorizationEphyList.add(produit);
        }

        return authorizationEphyList;
    }


}
