package com.mobileagri.detect.Service;

import com.mobileagri.detect.dao.DonneeDao;
import com.mobileagri.detect.dao.ParcelleDao;
import com.mobileagri.detect.dao.UserDao;
import com.mobileagri.detect.dao.WarningDao;
import com.mobileagri.detect.dao.product.*;
import com.mobileagri.detect.entity.*;
import com.mobileagri.detect.entity.product.Product;
import com.mobileagri.detect.utility.Exception.ParcelleNotFoundException;
import com.mobileagri.detect.utility.Exception.ProductNotFoundException;
import com.mobileagri.detect.utility.JSoupUtility;
import com.mobileagri.detect.utility.enums.Parcelles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 04/05/13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */
@Service("checkWarningService")
public class CheckWarningService {

    private final Logger logger = LoggerFactory.getLogger(CheckWarningService.class);

    @Autowired
    private DonneeDao donneeDao;

    @Autowired
    private EngraisDao engraisDao;

    @Autowired
    private HerbicideDao herbicideDao;

    @Autowired
    private InsecticideDao insecticideDao;

    @Autowired
    private FongicideDao fongicideDao;

    @Autowired
    private RegulateurDao regulateurDao;

    @Autowired
    private AzoteDao azoteDao;

    @Autowired
    private DiversDao diversDao;

    @Autowired
    private SouffreDao souffreDao;

    @Autowired
    private ParcelleDao parcelleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private WarningDao warningDao;

    public Product search(Integer id, String typeProduit) {


        Product p = null;
        switch (typeProduit.toLowerCase()) {
            case "herbicide":
                p = herbicideDao.selectById(id);
                break;
            case "azote":
                p = azoteDao.selectById(id);
                break;
            case "oligos":
                p = diversDao.selectById(id);
                break;
            case "engrais":
                p = engraisDao.selectById(id);
                break;
            case "fongicide":
                p = fongicideDao.selectById(id);
                break;
            case "insecticide":
                p = insecticideDao.selectById(id);
                break;
            case "regulateur":
                p = regulateurDao.selectById(id);
                break;
            case "souffre":
                p = souffreDao.selectById(id);
                break;
            default:
                logger.warn("produit non trouvé !" + id + " / " + typeProduit);
        }
        return p;

    }


    public void check() throws Exception {

        JSoupUtility jsoup = new JSoupUtility();
        EphyProduit ephyProduit;
        String amm = null;
        Product product;

        for (Donnee donnee : donneeDao.getAllWithCurrentCompaign()) {


            product = search(donnee.getIdProduit(), donnee.getTypeProduit());

            if (null == product) {
                throw new ProductNotFoundException("iD Produit : " + donnee.getIdProduit() + " / Type:" + donnee.getTypeProduit() + " \n" + donnee);
            }

            if (ammIsGood(product.getAmm())) {
                ephyProduit = jsoup.run(product.getAmm());

//                logger.info(donnee.toString());


                isWarning(donnee, ephyProduit);


            }


//            logger.info(donnee.getId().toString());

        }

    }

    private boolean ammIsGood(String amm) {
        return !("0".equals(amm) || "".equals(amm) || null == amm);
    }

    private WarningDonnee isWarning(Donnee donnee, EphyProduit ephyProduit) throws ParcelleNotFoundException {


        if (ephyProduit.isActive()) {
            List<EphyUsage> ephyUsages = ephyProduit.getListUsage();

            User user = userDao.selectUserByName(donnee.getUser());
            List<Parcelle> parcelleList = parcelleDao.getAll();
            Parcelle parcelle = parcelleDao.selectByName(donnee.getParcelle(), user.getId());
            Double ephyDose = 0.0;

            if (null != parcelle) {
//            logger.info(parcelle.getCulture() + ", Dose : " + donnee.getQuantite());
                logger.info(parcelle.getCulture());
                List<String> ephyLink = Parcelles.valueOf(parcelle.getCulture().toUpperCase()).getLinkEPhy();

                for (EphyUsage usage : ephyUsages) {
                    for (String s : ephyLink) {
                        logger.info(s.toUpperCase() + " / " + usage.getUsage().toUpperCase());
                        if (usage.getUsage().toUpperCase().contains(s.toUpperCase())) {
                            logger.info("TROUVE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                            logger.info(usage.getDose() + " / " + donnee.getQuantite());

                            if (isDouble(usage.getDose())) {
                                if (ephyDose < Double.valueOf(usage.getDose())) {
                                    ephyDose = Double.valueOf(usage.getDose());
                                }
                            }
                        }

                    }

//            logger.info(usage.getUsage() + ", dose : " + usage.getDose());
                }
                logger.info("##############Dose retenue : " + ephyDose);
                if (isDouble(donnee.getQuantite()) && ephyDose > 0) {
                    if (ephyDose < Double.valueOf(donnee.getQuantite())) {
                        logger.warn("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!WARNING!!!!!!!!!!!!!!!!!!!!!!");
                        WarningDonnee warning = new WarningDonnee();
                        warning.setDonnee(donnee);
                        warningDao.save(warning);
                    }
                }

            }
        }

        return null;
    }

    private boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
