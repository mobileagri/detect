package com.mobileagri.detect.Service;

import com.mobileagri.detect.dao.DonneeDao;
import com.mobileagri.detect.dao.WarningDao;
import com.mobileagri.detect.dao.product.*;
import com.mobileagri.detect.entity.Donnee;
import com.mobileagri.detect.entity.EphyProduit;
import com.mobileagri.detect.entity.User;
import com.mobileagri.detect.entity.product.*;
import com.mobileagri.detect.utility.JSoupUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.ConnectException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 04/05/13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */
@Service("donneeService")
public class DonneeService {

    private final Logger logger = LoggerFactory.getLogger(DonneeService.class);

    @Autowired
    private DonneeDao donneeDao;

    @Autowired
    private EngraisDao engraisDao;

    @Autowired
    private HerbicideDao herbicideDao;

    @Autowired
    private InsecticideDao insecticideDao;

    @Autowired
    private FongicideDao fongicideDao;

    @Autowired
    private RegulateurDao regulateurDao;

    @Autowired
    private AzoteDao azoteDao;

    @Autowired
    private DiversDao diversDao;

    @Autowired
    private SouffreDao souffreDao;

    @Autowired
    private WarningDao warningDao;


    public void convertAmm() throws ConnectException {
        System.out.println("check!");
        System.out.println("il y a :" + donneeDao.getAll().size());

        JSoupUtility jsoup = new JSoupUtility();
        EphyProduit ephyProduit = null;
        String amm;

        for (Herbicide h : herbicideDao.getAll()) {
            amm = extractAmm(jsoup, h.getAmm(), h.getName());
            if (null != amm) {
                h.setAmm(amm);
                herbicideDao.save(h);
            }
        }

        for (Insecticide h : insecticideDao.getAll()) {
            amm = extractAmm(jsoup, h.getAmm(), h.getName());
            if (null != amm) {
                h.setAmm(amm);
                insecticideDao.save(h);
            }
        }

        for (Fongicide h : fongicideDao.getAll()) {
            amm = extractAmm(jsoup, h.getAmm(), h.getName());
            if (null != amm) {
                h.setAmm(amm);
                fongicideDao.save(h);
            }
        }

        for (Divers h : diversDao.getAll()) {
            amm = extractAmm(jsoup, h.getAmm(), h.getName());
            if (null != amm) {
                h.setAmm(amm);
                diversDao.save(h);
            }
        }

        for (Regulateur h : regulateurDao.getAll()) {
            amm = extractAmm(jsoup, h.getAmm(), h.getName());
            if (null != amm) {
                h.setAmm(amm);
                regulateurDao.save(h);
            }
        }

        for (Engrais h : engraisDao.getAll()) {
            amm = extractAmm(jsoup, h.getAmm(), h.getName());
            if (null != amm) {
                h.setAmm(amm);
                engraisDao.save(h);
            }
        }


        ephyProduit = jsoup.run("2010537-1083");


        ephyProduit = jsoup.run("2010537-1083");
    }

    private String extractAmm(JSoupUtility jsoup, String amm, String name) throws ConnectException {
        if (null != amm && !amm.contains("-") && !amm.equals("0") && !amm.equals("")) {
            logger.info(amm + " / " + name);
            String newAmm = jsoup.convertAMMToLink(amm, name);
            logger.info("newAmm :" + newAmm);
            return newAmm;
        }

        return null;
    }

    public void insertIdProduit() {

        List<Donnee> donneeList = donneeDao.selectByNoIdDonnee();

        List<Donnee> engraisList = donneeDao.selectByTypeProduit("Engrais");
        List<Donnee> herbicideList = donneeDao.selectByTypeProduit("Herbicide");
        List<Donnee> insecticideList = donneeDao.selectByTypeProduit("Insecticide");
        List<Donnee> fongicideList = donneeDao.selectByTypeProduit("Fongicide");
        List<Donnee> regulateurList = donneeDao.selectByTypeProduit("Regulateur");
        List<Donnee> azoteList = donneeDao.selectByTypeProduit("Azote");
        List<Donnee> oligosList = donneeDao.selectByTypeProduit("Divers");
        List<Donnee> souffreList = donneeDao.selectByTypeProduit("Souffre");

        Engrais engrais = null;
        Herbicide herbicide = null;
        Insecticide insecticide = null;
        Fongicide fongicide = null;
        Regulateur regulateur = null;
        Azote azote = null;
        Divers divers = null;
        Souffre souffre = null;

        for (Donnee d : engraisList) {
            engrais = engraisDao.selectByName(d.getProduit());

            if (null != engrais) {
                d.setIdProduit(engrais.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : herbicideList) {
            herbicide = herbicideDao.selectByName(d.getProduit());

            if (null != herbicide) {
                d.setIdProduit(herbicide.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : insecticideList) {
            insecticide = insecticideDao.selectByName(d.getProduit());

            if (null != insecticide) {
                d.setIdProduit(insecticide.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : fongicideList) {
            fongicide = fongicideDao.selectByName(d.getProduit());

            if (null != fongicide) {
                d.setIdProduit(fongicide.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : regulateurList) {
            regulateur = regulateurDao.selectByName(d.getProduit());

            if (null != regulateur) {
                d.setIdProduit(regulateur.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : azoteList) {
            azote = azoteDao.selectByName(d.getProduit());

            if (null != azote) {
                d.setIdProduit(azote.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : oligosList) {
            divers = diversDao.selectByName(d.getProduit());

            if (null != divers) {
                d.setIdProduit(divers.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }

        for (Donnee d : souffreList) {
            souffre = souffreDao.selectByName(d.getProduit());

            if (null != souffre) {
                d.setIdProduit(souffre.getId());
            } else {
                System.out.println(d.getId());
            }
            donneeDao.save(d);
        }
    }

    public List<Donnee> selectDataByUser(User u) {
        return donneeDao.selectDataByUser(u.getLogin());
    }


}
