package com.mobileagri.detect.dao;


import com.mobileagri.detect.entity.WarningDonnee;

import java.util.List;

public interface WarningDao {
    public void save(WarningDonnee donnee);

    public List<WarningDonnee> getAll();

    public WarningDonnee selectById(Integer donnee);

    public void delete(WarningDonnee donnee);
}
