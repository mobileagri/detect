package com.mobileagri.detect.dao;


import com.mobileagri.detect.entity.Donnee;

import java.util.List;

public interface DonneeDao {
    public void save(Donnee donnee);

    public List<Donnee> getAll();

    public List<Donnee> getAllWithCurrentCompaign();

    public List<Donnee> selectByNoIdDonnee();

    public List<Donnee> selectByTypeProduit(String type);

    public Donnee selectById(Integer donnee);

    public void delete(Donnee donnee);

    public int getCampagneEnCours();

    public Object getProductById(String type, int id);

    public List<Donnee> selectDataByUser(String userLogin);
}
