package com.mobileagri.detect.dao;

import com.mobileagri.detect.entity.Parcelle;
import com.mobileagri.detect.utility.Exception.ParcelleNotFoundException;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("parcelleDao")
// Default is read only
@Transactional
public class ParcelleDAOImpl implements ParcelleDao {
    private HibernateTemplate hibernateTemplate;

    private final Logger logger = LoggerFactory.getLogger(ParcelleDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Parcelle user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Parcelle user) {
        hibernateTemplate.delete(user);

    }

    public List<Parcelle> getAll() {
        return (List<Parcelle>) hibernateTemplate.find("from Parcelle");
    }

    public int getNbParcelle() {
        return getAll().size();
    }

    public Parcelle selectById(Integer userId) {
        return hibernateTemplate.get(Parcelle.class, userId);
    }

    @Override
    public List<Parcelle> selectByUserId(Integer userId) {
        return (List<Parcelle>) hibernateTemplate.find("from Parcelle where id_client = " + userId);
    }

    @Override
    public List<Parcelle> selectByUserIdAndWithoutCulture(Integer userId) {
        return (List<Parcelle>) hibernateTemplate.find("from Parcelle where id_client = " + userId + " and culture=''");
    }

    @Override
    public List<Parcelle> selectByUserIdAndWithCulture(Integer userId) {
        return (List<Parcelle>) hibernateTemplate.find("from Parcelle where id_client = " + userId + " and culture!='' and culture!='GEL'");
    }

    public Parcelle selectByName(String name, Integer idClient) throws ParcelleNotFoundException {
        Parcelle p;
        try {
            p = (Parcelle) hibernateTemplate.find("from Parcelle where name = '" + name + "' and idClient = " + idClient).get(0);
        } catch (Exception e) {
            throw new ParcelleNotFoundException("Parcelle : " + name + " non trouvée !");
        }

        return p;
    }

    @Override
    public List<Parcelle> selectByObjectifRendement(Integer userId) {
        return (List<Parcelle>) hibernateTemplate.find("from Parcelle where id_client = " + userId + " and objectif_rendement>0 and culture!='' and culture!='GEL'");
    }


}
