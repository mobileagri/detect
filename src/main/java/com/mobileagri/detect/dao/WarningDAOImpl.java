package com.mobileagri.detect.dao;

import com.mobileagri.detect.entity.Donnee;
import com.mobileagri.detect.entity.WarningDonnee;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("warningDao")
// Default is read only
@Transactional
public class WarningDAOImpl implements WarningDao {
    private HibernateTemplate hibernateTemplate;

    @Autowired
    private SessionFactory sessionFactory;

    //
//
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(WarningDonnee user) {
//        selectByDonnee(user.getDonnee());
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(WarningDonnee user) {
        hibernateTemplate.delete(user);

    }

    public List<WarningDonnee> getAll() {
        return (List<WarningDonnee>) hibernateTemplate.find("from WarningDonnee");
    }

    public WarningDonnee selectById(Integer userId) {
        return hibernateTemplate.get(WarningDonnee.class, userId);
    }

    public WarningDonnee selectByDonnee(Donnee donnee) {
        return (WarningDonnee) hibernateTemplate.find("from WarningDonnee where donnee = " + donnee).get(0);
    }


}
