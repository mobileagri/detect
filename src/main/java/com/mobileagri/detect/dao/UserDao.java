package com.mobileagri.detect.dao;

import com.mobileagri.detect.entity.User;

import java.util.List;

public interface UserDao {
    public void saveUser(User user);

    public List<User> getAllUser();

    public List<User> getAllAgriUser();

    public User selectUserById(int userId);

    public User selectUserByName(String name);

    public void deleteUser(User user);
}
