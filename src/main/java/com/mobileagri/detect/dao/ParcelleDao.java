package com.mobileagri.detect.dao;


import com.mobileagri.detect.entity.Parcelle;
import com.mobileagri.detect.utility.Exception.ParcelleNotFoundException;

import java.util.List;

public interface ParcelleDao {

    public void save(Parcelle parcelle);

    public List<Parcelle> getAll();

    public int getNbParcelle();

    public Parcelle selectById(Integer parcelle);

    public List<Parcelle> selectByUserId(Integer userId);

    public List<Parcelle> selectByUserIdAndWithoutCulture(Integer userId);

    public List<Parcelle> selectByUserIdAndWithCulture(Integer userId);

    public Parcelle selectByName(String name, Integer idClient) throws ParcelleNotFoundException;

    public List<Parcelle> selectByObjectifRendement(Integer userId);

    public void delete(Parcelle parcelle);

}
