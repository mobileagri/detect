package com.mobileagri.detect.dao;

import com.mobileagri.detect.entity.Donnee;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("donneeDao")
// Default is read only
@Transactional
public class DonneeDAOImpl implements DonneeDao {
    private HibernateTemplate hibernateTemplate;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Donnee user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Donnee user) {
        hibernateTemplate.delete(user);

    }

    public int getCampagneEnCours() {
        return (Integer) sessionFactory.getCurrentSession().createSQLQuery("SELECT campagneEnCours FROM configuration").list().get(0);
    }

    public Object getProductById(String type, int id) {
        String query = "SELECT * FROM " + type + " WHERE id_nom" + type + " = " + id + "";
        return sessionFactory.getCurrentSession().createSQLQuery(query).list().get(0);
    }

    @Override
    public List<Donnee> selectDataByUser(String userLogin) {
        return (List<Donnee>) hibernateTemplate.find("from Donnee where user = '" + userLogin + "'");
    }

    @SuppressWarnings("unchecked")
    public List<Donnee> getAll() {
        return (List<Donnee>) hibernateTemplate.find("from Donnee");
    }

    @SuppressWarnings("unchecked")
    public List<Donnee> getAllWithCurrentCompaign() {
        return (List<Donnee>) hibernateTemplate.find("FROM Donnee WHERE user LIKE '%" + getCampagneEnCours() + "'");
    }

    public List<Donnee> selectByNoIdDonnee() {
        return (List<Donnee>) hibernateTemplate.find("from Donnee where idProduit = 0");
    }

    public List<Donnee> selectByTypeProduit(String type) {
        return (List<Donnee>) hibernateTemplate.find("from Donnee where typeProduit = ? and idProduit = 0 and user LIKE '%" + getCampagneEnCours() + "'", type);
    }

    public Donnee selectById(Integer userId) {
        return hibernateTemplate.get(Donnee.class, userId);
    }


}
