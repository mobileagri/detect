package com.mobileagri.detect.dao;

import com.mobileagri.detect.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

//This will make easier to autowired
@Repository("userDao")
// Default is read only
@Transactional
public class UserDaoImpl implements UserDao {
    private HibernateTemplate hibernateTemplate;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void saveUser(User user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void deleteUser(User user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<User> getAllUser() {
        return (List<User>) hibernateTemplate.find("from User");
    }

    public User selectUserById(int userId) {
        return hibernateTemplate.get(User.class, userId);
    }

    public User selectUserByName(String name) {
        return (User) hibernateTemplate.find("from User where login = '" + name + "'").get(0);
    }

    @Override
    public List<User> getAllAgriUser() {
        List<User> userList = (List<User>) hibernateTemplate.find("from User where privilege=2 ");
        List<User> userListReturn = new ArrayList<>();

        //Remove user like Duyck2, without year
        String login;
        for (User user : userList) {
            login = user.getLogin();
            if (login.contains("20")) {
                userListReturn.add(user);
            }
        }

        return userListReturn;
    }

}
