package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Souffre;

import java.util.List;

public interface SouffreDao {
    public void save(Souffre engrais);

    public List<Souffre> getAll();

    public Souffre selectById(Integer engrais);

    public Souffre selectByName(String name);

    public void delete(Souffre engrais);
}
