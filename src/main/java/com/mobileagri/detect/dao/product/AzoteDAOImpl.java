package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Azote;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("azoteDao")
// Default is read only
@Transactional
public class AzoteDAOImpl implements AzoteDao {

    private final Logger logger = LoggerFactory.getLogger(AzoteDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Azote user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Azote user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Azote> getAll() {
        return (List<Azote>) hibernateTemplate.find("from Azote");
    }

    public Azote selectById(Integer userId) {
        return hibernateTemplate.get(Azote.class, userId);
    }

    public Azote selectByName(String name) {
//        return hibernateTemplate.get(Engrais.class, userId);

        List<Azote> produitList = hibernateTemplate.find("from Azote e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
