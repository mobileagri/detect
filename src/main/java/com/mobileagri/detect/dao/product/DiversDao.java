package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Divers;

import java.util.List;

public interface DiversDao {
    public void save(Divers engrais);

    public List<Divers> getAll();

    public Divers selectById(Integer engrais);

    public Divers selectByName(String name);

    public void delete(Divers engrais);
}
