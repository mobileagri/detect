package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Insecticide;

import java.util.List;

public interface InsecticideDao {
    public void save(Insecticide engrais);

    public List<Insecticide> getAll();

    public Insecticide selectById(Integer engrais);

    public Insecticide selectByName(String name);

    public void delete(Insecticide engrais);
}
