package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Fongicide;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("fongicideDao")
// Default is read only
@Transactional
public class FongicideDAOImpl implements FongicideDao {

    private final Logger logger = LoggerFactory.getLogger(FongicideDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Fongicide user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Fongicide user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Fongicide> getAll() {
        return (List<Fongicide>) hibernateTemplate.find("from Fongicide");
    }

    public Fongicide selectById(Integer userId) {
        return hibernateTemplate.get(Fongicide.class, userId);
    }

    public Fongicide selectByName(String name) {
//        return hibernateTemplate.get(Engrais.class, userId);

        List<Fongicide> produitList = hibernateTemplate.find("from Fongicide e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
