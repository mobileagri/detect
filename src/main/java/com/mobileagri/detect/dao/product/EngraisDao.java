package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Engrais;

import java.util.List;

public interface EngraisDao {
    public void save(Engrais engrais);

    public List<Engrais> getAll();

    public Engrais selectById(Integer engrais);

    public Engrais selectByName(String name);

    public void delete(Engrais engrais);
}
