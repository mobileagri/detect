package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Divers;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("oligosDao")
// Default is read only
@Transactional
public class DiversDAOImpl implements DiversDao {

    private final Logger logger = LoggerFactory.getLogger(DiversDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Divers user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Divers user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Divers> getAll() {
        return (List<Divers>) hibernateTemplate.find("from Divers");
    }

    public Divers selectById(Integer userId) {
        return hibernateTemplate.get(Divers.class, userId);
    }

    public Divers selectByName(String name) {
//        return hibernateTemplate.get(Engrais.class, userId);

        List<Divers> produitList = hibernateTemplate.find("from Divers e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
