package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Azote;

import java.util.List;

public interface AzoteDao {
    public void save(Azote engrais);

    public List<Azote> getAll();

    public Azote selectById(Integer engrais);

    public Azote selectByName(String name);

    public void delete(Azote engrais);
}
