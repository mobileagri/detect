package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Regulateur;

import java.util.List;

public interface RegulateurDao {
    public void save(Regulateur engrais);

    public List<Regulateur> getAll();

    public Regulateur selectById(Integer engrais);

    public Regulateur selectByName(String name);

    public void delete(Regulateur engrais);
}
