package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Engrais;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("engraisDao")
// Default is read only
@Transactional
public class EngraisDAOImpl implements EngraisDao {

    private final Logger logger = LoggerFactory.getLogger(EngraisDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Engrais user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Engrais user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Engrais> getAll() {
        return (List<Engrais>) hibernateTemplate.find("from Engrais");
    }

    public Engrais selectById(Integer userId) {
        return hibernateTemplate.get(Engrais.class, userId);
    }

    public Engrais selectByName(String name) {

        List<Engrais> produitList = hibernateTemplate.find("from Engrais e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);

            return null;
        }
        return produitList.get(0);
    }


}
