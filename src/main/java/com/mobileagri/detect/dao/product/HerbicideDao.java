package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Herbicide;

import java.util.List;

public interface HerbicideDao {
    public void save(Herbicide engrais);

    public List<Herbicide> getAll();

    public Herbicide selectById(Integer engrais);

    public Herbicide selectByName(String name);

    public void delete(Herbicide engrais);
}
