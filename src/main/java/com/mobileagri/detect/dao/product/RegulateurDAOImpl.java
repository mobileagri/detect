package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Regulateur;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("regulateurDao")
// Default is read only
@Transactional
public class RegulateurDAOImpl implements RegulateurDao {

    private final Logger logger = LoggerFactory.getLogger(RegulateurDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Regulateur user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Regulateur user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Regulateur> getAll() {
        return (List<Regulateur>) hibernateTemplate.find("from Regulateur");
    }

    public Regulateur selectById(Integer userId) {
        return hibernateTemplate.get(Regulateur.class, userId);
    }

    public Regulateur selectByName(String name) {
//        return hibernateTemplate.get(Engrais.class, userId);

        List<Regulateur> produitList = hibernateTemplate.find("from Regulateur e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
