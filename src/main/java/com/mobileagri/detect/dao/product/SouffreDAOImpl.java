package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Souffre;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("souffreDao")
// Default is read only
@Transactional
public class SouffreDAOImpl implements SouffreDao {

    private final Logger logger = LoggerFactory.getLogger(SouffreDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Souffre user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Souffre user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Souffre> getAll() {
        return (List<Souffre>) hibernateTemplate.find("from Souffre");
    }

    public Souffre selectById(Integer userId) {
        return hibernateTemplate.get(Souffre.class, userId);
    }

    public Souffre selectByName(String name) {
//        return hibernateTemplate.get(Engrais.class, userId);

        List<Souffre> produitList = hibernateTemplate.find("from Souffre e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
