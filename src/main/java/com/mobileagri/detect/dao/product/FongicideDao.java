package com.mobileagri.detect.dao.product;


import com.mobileagri.detect.entity.product.Fongicide;

import java.util.List;

public interface FongicideDao {
    public void save(Fongicide engrais);

    public List<Fongicide> getAll();

    public Fongicide selectById(Integer engrais);

    public Fongicide selectByName(String name);

    public void delete(Fongicide engrais);
}
