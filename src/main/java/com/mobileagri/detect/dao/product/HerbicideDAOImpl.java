package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Herbicide;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("herbicideDao")
// Default is read only
@Transactional
public class HerbicideDAOImpl implements HerbicideDao {

    private final Logger logger = LoggerFactory.getLogger(HerbicideDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Herbicide user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Herbicide user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Herbicide> getAll() {
        return (List<Herbicide>) hibernateTemplate.find("from Herbicide");
    }

    public Herbicide selectById(Integer userId) {
        return hibernateTemplate.get(Herbicide.class, userId);
    }

    public Herbicide selectByName(String name) {
//        return hibernateTemplate.get(Engrais.class, userId);

        List<Herbicide> produitList = hibernateTemplate.find("from Herbicide e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
