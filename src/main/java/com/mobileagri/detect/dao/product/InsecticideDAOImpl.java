package com.mobileagri.detect.dao.product;

import com.mobileagri.detect.entity.product.Insecticide;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//This will make easier to autowired
@Repository("insecticideDao")
// Default is read only
@Transactional
public class InsecticideDAOImpl implements InsecticideDao {

    private final Logger logger = LoggerFactory.getLogger(InsecticideDAOImpl.class);

    private HibernateTemplate hibernateTemplate;

    //    @Autowired
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        hibernateTemplate = new HibernateTemplate(sessionFactory);
    }

    @Transactional(readOnly = false)
    public void save(Insecticide user) {
        hibernateTemplate.saveOrUpdate(user);

    }

    @Transactional(readOnly = false)
    public void delete(Insecticide user) {
        hibernateTemplate.delete(user);

    }

    @SuppressWarnings("unchecked")
    public List<Insecticide> getAll() {
        return (List<Insecticide>) hibernateTemplate.find("from Insecticide");
    }

    public Insecticide selectById(Integer userId) {
        return hibernateTemplate.get(Insecticide.class, userId);
    }

    public Insecticide selectByName(String name) {

        List<Insecticide> produitList = hibernateTemplate.find("from Insecticide e where e.name = ?", name);
        if (produitList.size() == 0) {
            logger.warn(name);
            return null;
        }
        return produitList.get(0);
    }


}
