package com.mobileagri.detect.app;

import com.mobileagri.detect.Service.AppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 04/05/13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
public class App {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(App.class);

        ApplicationContext context = new ClassPathXmlApplicationContext("/app-config.xml");
        AppService appService = (AppService) context.getBean("appService");

        if (args.length > 0 && args[0].equals("avancement"))
            appService.checkAvancement();

        if (args.length > 0 && args[0].equals("warning")) {
            if (args.length == 2) {
                appService.checkWarningData(args[1]);
            } else {
                logger.error("Veuillez entrer le chemin du fichier csv après " + args[0]);
            }
        }

//        appService.checkWarningData();
    }
}
