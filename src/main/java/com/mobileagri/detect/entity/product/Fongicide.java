package com.mobileagri.detect.entity.product;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "fongicide")
public class Fongicide extends Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_nomFongicide")
    private Integer id;

    @Column(name = "nomFongicide")
    private String name;

    @Column(name = "amm")
    private String amm;

    @Column(name = "statut")
    private String statut;

    @Column(name = "date_retrait")
    private String date_retrait;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmm() {
        return amm;
    }

    public void setAmm(String amm) {
        this.amm = amm;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getDate_retrait() {
        return date_retrait;
    }

    public void setDate_retrait(String date_retrait) {
        this.date_retrait = date_retrait;
    }
}
