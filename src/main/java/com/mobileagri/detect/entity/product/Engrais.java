package com.mobileagri.detect.entity.product;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "engrais")
public class Engrais extends Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_nomEngrais")
    private Integer id;

    @Column(name = "nomEngrais")
    private String name;

    @Column(name = "amm")
    private String amm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmm() {
        return amm;
    }

    public void setAmm(String amm) {
        this.amm = amm;
    }

}
