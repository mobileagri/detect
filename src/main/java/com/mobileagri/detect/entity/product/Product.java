package com.mobileagri.detect.entity.product;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 12/05/13
 * Time: 15:17
 * To change this template use File | Settings | File Templates.
 */
public class Product {

    private Integer id;

    private String name;

    private String amm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmm() {
        return amm;
    }

    public void setAmm(String amm) {
        this.amm = amm;
    }
}
