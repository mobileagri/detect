package com.mobileagri.detect.entity;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "nomparcelle")
public class Parcelle implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_nomParcelle")
    private Integer id;

    @Column(name = "nomParcelle")
    private String name;

    @Column(name = "id_client")
    private Integer idClient;

    @Column(name = "ilot")
    private Integer ilot;

    @Column(name = "culture")
    private String culture;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getIlot() {
        return ilot;
    }

    public void setIlot(Integer ilot) {
        this.ilot = ilot;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }
}
