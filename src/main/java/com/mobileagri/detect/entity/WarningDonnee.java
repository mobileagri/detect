package com.mobileagri.detect.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;


@Entity
@Table(name = "warning")
public class WarningDonnee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;


    @ManyToOne
    @JoinColumn(name = "donnee_id", unique = true)
    private Donnee donnee;

    @Column(name = "date_ajout_db")
    private Date date_ajout_db;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Donnee getDonnee() {
        return donnee;
    }

    public void setDonnee(Donnee donnee) {
        this.donnee = donnee;
    }

    public Date getDate_ajout_db() {
        return date_ajout_db;
    }

    public void setDate_ajout_db(Date date_ajout_db) {
        this.date_ajout_db = date_ajout_db;
    }

}
