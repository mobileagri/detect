package com.mobileagri.detect.entity;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 05/05/13
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
public class EphyProduit {

    private String amm;
    private String name;
    private boolean isActive;
    private List<EphyUsage> listUsage;

    public String getAmm() {
        return amm;
    }

    public void setAmm(String amm) {
        this.amm = amm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EphyUsage> getListUsage() {
        return listUsage;
    }

    public void setListUsage(List<EphyUsage> listUsage) {
        this.listUsage = listUsage;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "EphyProduit{" +
                "amm='" + amm + '\'' +
                ", name='" + name + '\'' +
                ", isActive=" + isActive +
                ", listUsage=" + listUsage +
                '}';
    }
}
