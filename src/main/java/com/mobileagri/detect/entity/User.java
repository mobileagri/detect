package com.mobileagri.detect.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "utilisateurs")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_user")
    private Integer id;

    @Column(name = "login")
    private String login;

    @Column(name = "avancement")
    private Double avancement;

    @Column(name = "avancement_assolement")
    private Double avancementAssolement;

    @Column(name = "avancement_carnet")
    private Double avancementCarnet;

    @Column(name = "avancement_fumure")
    private Double avancementFumure;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Double getAvancement() {
        return avancement;
    }

    public void setAvancement(Double avancement) {
        this.avancement = avancement;
    }

    public Double getAvancementAssolement() {
        return avancementAssolement;
    }

    public void setAvancementAssolement(Double avancementAssolement) {
        this.avancementAssolement = avancementAssolement;
    }

    public Double getAvancementCarnet() {
        return avancementCarnet;
    }

    public void setAvancementCarnet(Double avancementCarnet) {
        this.avancementCarnet = avancementCarnet;
    }

    public Double getAvancementFumure() {
        return avancementFumure;
    }

    public void setAvancementFumure(Double avancementFumure) {
        this.avancementFumure = avancementFumure;
    }
}
