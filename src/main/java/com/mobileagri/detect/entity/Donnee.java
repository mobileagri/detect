package com.mobileagri.detect.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;


@Entity
@Table(name = "donnees")
public class Donnee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_donnees")
    private Integer id;

    @Column(name = "parcelle")
    private String parcelle;

    @Column(name = "type_produit")
    private String typeProduit;

    @Column(name = "produit")
    private String produit;

    @Column(name = "idProduit")
    private Integer idProduit;

    @Column(name = "quantite")
    private String quantite;

    @Column(name = "date")
    private Date dateDonnee;

    @Column(name = "user")
    private String user;

    @Column(name = "prix")
    private String prix;

//    @OneToMany(mappedBy="warning")
//    private Set<WarningDonnee> warnings;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParcelle() {
        return parcelle;
    }

    public void setParcelle(String parcelle) {
        this.parcelle = parcelle;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public Date getDateDonnee() {
        return dateDonnee;
    }

    public void setDateDonnee(Date dateDonnee) {
        this.dateDonnee = dateDonnee;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public Integer getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public String getTypeProduit() {
        return typeProduit;
    }

    public void setTypeProduit(String typeProduit) {
        this.typeProduit = typeProduit;
    }

//    public Set<WarningDonnee> getWarnings() {
//        return warnings;
//    }
//
//    public void setWarnings(Set<WarningDonnee> warnings) {
//        this.warnings = warnings;
//    }


    @Override
    public String toString() {
        return "Donnee{" +
                "id=" + id +
                ", parcelle='" + parcelle + '\'' +
                ", typeProduit='" + typeProduit + '\'' +
                ", produit='" + produit + '\'' +
                ", idProduit=" + idProduit +
                ", quantite='" + quantite + '\'' +
                ", dateDonnee=" + dateDonnee +
                ", user='" + user + '\'' +
                ", prix='" + prix + '\'' +
                '}';
    }
}
