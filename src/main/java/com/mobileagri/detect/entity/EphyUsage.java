package com.mobileagri.detect.entity;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 05/05/13
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */
public class EphyUsage {

    private boolean authorization;
    private String dose;
    private String unite;
    private String usage;
    private String date;
    private String maxAppli;
    private String dar;
    private String iznt;
    private String delaiCommerc;
    private String delaiUtilisation;

    public boolean isAuthorization() {
        return authorization;
    }


    public void setAuthorization(boolean authorization) {
        this.authorization = authorization;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMaxAppli() {
        return maxAppli;
    }

    public void setMaxAppli(String maxAppli) {
        this.maxAppli = maxAppli;
    }

    public String getDar() {
        return dar;
    }

    public void setDar(String dar) {
        this.dar = dar;
    }

    public String getIznt() {
        return iznt;
    }

    public void setIznt(String iznt) {
        this.iznt = iznt;
    }

    public String getDelaiCommerc() {
        return delaiCommerc;
    }

    public void setDelaiCommerc(String delaiCommerc) {
        this.delaiCommerc = delaiCommerc;
    }

    public String getDelaiUtilisation() {
        return delaiUtilisation;
    }

    public void setDelaiUtilisation(String delaiUtilisation) {
        this.delaiUtilisation = delaiUtilisation;
    }

    @Override
    public String toString() {
        return "EphyUsage{" +
                "authorization=" + authorization +
                ", dose='" + dose + '\'' +
                ", unite='" + unite + '\'' +
                ", usage='" + usage + '\'' +
                ", date='" + date + '\'' +
                ", maxAppli='" + maxAppli + '\'' +
                ", dar='" + dar + '\'' +
                ", iznt='" + iznt + '\'' +
                ", delaiCommerc='" + delaiCommerc + '\'' +
                ", delaiUtilisation='" + delaiUtilisation + '\'' +
                '}';
    }
}
