package com.mobileagri.detect.entity;

import java.io.Serializable;

public class AuthorizationEphy implements Serializable {

    private String numAmm;
    private String datePremiereAuthorisation;
    private String dateEcheanceAuthorization;
    private String dateRetraitDefinitif;
    private String statut;
    private String nomCommercial;
    private String typeCommercial;
    private String entreprise;

    public AuthorizationEphy() {
    }

    public AuthorizationEphy(String numAmm, String datePremiereAuthorisation, String dateEcheanceAuthorization, String dateRetraitDefinitif, String statut, String nomCommercial, String typeCommercial, String entreprise) {
        this.numAmm = numAmm;
        this.datePremiereAuthorisation = datePremiereAuthorisation;
        this.dateEcheanceAuthorization = dateEcheanceAuthorization;
        this.dateRetraitDefinitif = dateRetraitDefinitif;
        this.statut = statut;
        this.nomCommercial = nomCommercial;
        this.typeCommercial = typeCommercial;
        this.entreprise = entreprise;
    }

    public String getNumAmm() {
        return numAmm;
    }

    public void setNumAmm(String numAmm) {
        this.numAmm = numAmm;
    }

    public String getDatePremiereAuthorisation() {
        return datePremiereAuthorisation;
    }

    public void setDatePremiereAuthorisation(String datePremiereAuthorisation) {
        this.datePremiereAuthorisation = datePremiereAuthorisation;
    }

    public String getDateEcheanceAuthorization() {
        return dateEcheanceAuthorization;
    }

    public void setDateEcheanceAuthorization(String dateEcheanceAuthorization) {
        this.dateEcheanceAuthorization = dateEcheanceAuthorization;
    }

    public String getDateRetraitDefinitif() {
        return dateRetraitDefinitif;
    }

    public void setDateRetraitDefinitif(String dateRetraitDefinitif) {
        this.dateRetraitDefinitif = dateRetraitDefinitif;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getNomCommercial() {
        return nomCommercial;
    }

    public void setNomCommercial(String nomCommercial) {
        this.nomCommercial = nomCommercial;
    }

    public String getTypeCommercial() {
        return typeCommercial;
    }

    public void setTypeCommercial(String typeCommercial) {
        this.typeCommercial = typeCommercial;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }
}
