package com.mobileagri.detect.utility.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 05/05/13
 * Time: 19:50
 * To change this template use File | Settings | File Templates.
 */
public enum Parcelles {

    BETTERAVE("check", "test2", "ZONES CULTIVEES TOUTES CULTURES"),
    BLE("BLE", " CEREALES", "ZONES CULTIVEES TOUTES CULTURES", "ADJUVANTS"),
    COLZA("COLZA", "ZONES CULTIVEES TOUTES CULTURES", "CRUCIFERES OLEAGINEUSES"),
    ESCOURGEON("CEREALES", "ORGE D'HIVER", "ZONES CULTIVEES TOUTES CULTURES"),
    FEVEROLE("FEVEROLES", "ZONES CULTIVEES TOUTES CULTURES"),
    GEL("gel"),
    LIN("LIN", "ZONES CULTIVEES TOUTES CULTURES"),
    MAIS("MAIS", "ZONES CULTIVEES TOUTES CULTURES"),
    ORGE("ORGE", "ORGE DE PRINTEMPS", "ZONES CULTIVEES TOUTES CULTURES"),
    POIS("POIS", "ZONES CULTIVEES TOUTES CULTURES"),
    PRAIRIE("PRAIRIES"),
    TOURNESOL("TOURNESOL", "ZONES CULTIVEES TOUTES CULTURES");


    private final List<String> linkEPhy;

    private Parcelles(String... param) {

        linkEPhy = new ArrayList<>();

        Collections.addAll(linkEPhy, param);

    }

    public List<String> getLinkEPhy() {
        return linkEPhy;
    }
}
