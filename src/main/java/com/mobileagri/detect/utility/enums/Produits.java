package com.mobileagri.detect.utility.enums;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 05/05/13
 * Time: 19:50
 * To change this template use File | Settings | File Templates.
 */
public enum Produits {
    Azote("Azote"),
    Divers("Divers"),
    Engrais("Engrais"),
    Fongicide("Fongicide"),
    Herbicide("Herbicide"),
    Insecticide("Insecticide"),
    Regulateur("Regulateur"),
    Souffre("Souffre");


    private String name;

    private Produits(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
