package com.mobileagri.detect.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 08/05/13
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
public class MProperty {

    private static MProperty _instance;

    private static Properties prop;

    private MProperty() {
        try {
            prop = new Properties();
            prop.load(new FileInputStream("src/main/resources/configuration.properties"));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static MProperty getInstance() {
        if (null == _instance) {
            _instance = new MProperty();
        }
        return _instance;
    }

    public Properties getProp() {
        return prop;
    }
}
