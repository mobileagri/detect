package com.mobileagri.detect.utility;

import com.mobileagri.detect.entity.EphyProduit;
import com.mobileagri.detect.entity.EphyUsage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: romain_lou
 * Date: 01/05/13
 * Time: 19:17
 * To change this template use File | Settings | File Templates.
 */
public class JSoupUtility {

    private final Logger logger = LoggerFactory.getLogger(JSoupUtility.class);

    public String convertAMMToLink(String amm, String name) throws ConnectException {
        if (StringUtils.isBlank(amm) || amm.contains("-")) {
            throw new IllegalArgumentException("AMM is required OR AMM is not in the good format!");
        }
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Name is required!");
        }


        Document doc;
        try {
            doc = Jsoup.connect("http://e-phy.agriculture.gouv.fr/spe/amm/" + amm + ".htm").get();
        } catch (IOException e) {
            //Impossible de se connecter au site HTML
            throw new ConnectException("http://e-phy.agriculture.gouv.fr/spe/amm/" + amm + ".htm");
        }


        if (doc.getElementsByTag("table").size() > 0) {
            Element tab = doc.getElementsByTag("table").get(0);


            List<Element> listProduit = tab.getElementsByTag("a");

            for (Element e : listProduit) {
                if ((name.replace(" ", "").toUpperCase().contains(e.text().replace(" ", "").toUpperCase())) || (e.text().replace(" ", "").toUpperCase().contains(name.replace(" ", "").toUpperCase()))) {
                    return convertToLink(e.attr("href"));
                }
            }
        } else if (doc.getElementsByTag("meta").size() > 0) {
            for (Element meta : doc.getElementsByTag("meta")) {
                if ("refresh".equals(meta.attr("http-equiv"))) {
                    return convertToLink(meta.attr("content"));
                }
            }
        }


        return null;

    }

    private String convertToLink(String link) {
        link = link.substring(link.indexOf("/") + 1, link.length() - 4);
        return link;
    }

    public EphyProduit run(String amm) throws ConnectException {

        if (StringUtils.isBlank(amm)) {
            throw new IllegalArgumentException("AMM is required !");
        }

        EphyProduit ephyProduit = new EphyProduit();
        ephyProduit.setAmm(amm);
        ephyProduit.setActive(true);

        List<EphyUsage> usageList = new ArrayList<>();
        EphyUsage usage;

        Document doc;

        String link = "http://e-phy.agriculture.gouv.fr/spe/" + amm + ".htm";
        try {
            logger.info(link);
            doc = Jsoup.connect(link).get();
        } catch (IOException e) {
            //Impossible de se connecter au site HTML
            throw new ConnectException(link);
        }

        if (isActive(doc)) {
            ephyProduit.setActive(false);
            return ephyProduit;
        }

        //Initialisation des différentes listes d'element

        List<Element> listPtPolice = doc.getElementsByClass("petitepolice");
        List<Element> listLigne = null;
        List<Element> listColonne;

        //Récupération du bloc avec toutes les doses
        for (Element el : listPtPolice) {
            if (el.text().contains("Dose")) {
                listLigne = el.getElementsByTag("tr");
            }
        }

        for (Element elLigne : listLigne) {

            listColonne = elLigne.getElementsByTag("td");
            Integer size = listColonne.size();
            Integer size2 = null;
            if (listColonne.size() > 1) {
                usage = new EphyUsage();
                if (listColonne.get(0).getElementsByTag("img").size() > 0) {
                    usage.setAuthorization(isAuthorized(listColonne.get(0).getElementsByTag("img").get(0).toString()));
                }
                usage.setDose(listColonne.get(1).text());
                usage.setUnite(listColonne.get(2).text());
                usage.setUsage(listColonne.get(4).text());
                usage.setDate(listColonne.get(5).text());
                usage.setMaxAppli(listColonne.get(6).text());
                usage.setDar(listColonne.get(7).text());
                usage.setIznt(listColonne.get(8).text());
                usage.setDelaiCommerc(listColonne.get(9).text());
                usage.setDelaiUtilisation(listColonne.get(10).text());

                usageList.add(usage);
            }
        }

        ephyProduit.setListUsage(usageList);
        System.out.println(ephyProduit);

        return ephyProduit;
    }


    private boolean isAuthorized(String image) {
        if (image.contains("a_vert"))
            return true;
        else if (image.contains("f_rouge"))
            return false;
        return false;
    }

    private boolean isActive(Document html) {
        return "titremajusculerouge".equals(html.getElementsByTag("div").get(0).attr("class"));
    }

}
