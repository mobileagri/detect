package com.mobileagri.detect.utility.Exception;

/**
 * Created with IntelliJ IDEA.
 * User: louvet
 * Date: 20/05/13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */
public class ProductNotFoundException extends Exception {

    public ProductNotFoundException(String message) {
        super(message);
    }
}
