package utility;

import com.mobileagri.detect.entity.EphyProduit;
import com.mobileagri.detect.utility.JSoupUtility;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.ConnectException;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/app-config.xml"})
public class JSoupUtilityUnit {
    private final JSoupUtility jSoupUtility;

    public JSoupUtilityUnit() {
        this.jSoupUtility = new JSoupUtility();
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_return_illegalArgumentException_convertAMM() throws ConnectException {

        assertThat(jSoupUtility.convertAMMToLink("", "12331"))
                .isInstanceOf(IllegalArgumentException.class);

        assertThat(jSoupUtility.convertAMMToLink("12331", ""))
                .isInstanceOf(IllegalArgumentException.class);

        assertThat(jSoupUtility.convertAMMToLink("", ""))
                .isInstanceOf(IllegalArgumentException.class);

        assertThat(jSoupUtility.convertAMMToLink(null, null))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test(expected = ConnectException.class)
    public void should_search_unexisting_amm_convertAMM() throws ConnectException {

        assertThat(jSoupUtility.convertAMMToLink("123456", "12331"))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void should_search_existing_amm_convertAMM() throws ConnectException {

        assertThat(jSoupUtility.convertAMMToLink("9900191", "BLE")).isEqualTo("9900191-18608");
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_return_illegalArgumentException_run() throws ConnectException {

        assertThat(jSoupUtility.run(""))
                .isInstanceOf(IllegalArgumentException.class);

        assertThat(jSoupUtility.run(null))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void should_return_run() throws ConnectException {

        assertThat(jSoupUtility.run("9900191-18608"))
                .isNotNull()
                .isInstanceOf(EphyProduit.class);
    }

}
