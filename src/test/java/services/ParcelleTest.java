package services;

import com.mobileagri.detect.Service.ParcelleService;
import com.mobileagri.detect.Service.UserService;
import com.mobileagri.detect.entity.User;
import org.fest.assertions.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/app-config.xml"})
public class ParcelleTest {
    private ParcelleService parcelleService;
    private UserService userService;

    @Autowired
    public void setParcelleService(ParcelleService parcelleService) {
        this.parcelleService = parcelleService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Test
    public void should_check_assolement() {
        Double d;
        for (User user : userService.getAllAgriUser()) {
            d = parcelleService.checkAssolement(user);
            Assertions.assertThat(d)
                    .isGreaterThanOrEqualTo(0)
                    .isLessThanOrEqualTo(100);
        }
    }
}
